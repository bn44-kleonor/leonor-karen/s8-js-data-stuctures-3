/*============================================
GUIDED ACTIVITY

MODULE: WD004-S8-DATA-STRUCTURE-3

RECURSION
A programming term that means calling a function from itself.

Example:
Factorial of a number n is the product of all positive
integers less than or equal to n. Hence,

4! = 4 x 3 x 2 x 1 = 24
============================================*/


let num = 4;
let tempElem = 1;
for(let x=1; x <= num; x++) {
	console.log(tempElem + " x " + x);
	tempElem = tempElem * x;		// or syntax:   tempElement *= x;	
}														
console.log(num + "! (factorial):  " + tempElem);



function factorial(x) {
	if(x > 1){
		return x * factorial(x-1);	// a function that calls itself
			//2 x 1					// return is a statement that ends function execution and returns value
			//3 x 2					// this is the 'recursion' (repeated application of a rule)
			//4 x 3
	}
	return x;
}

console.log(factorial(4));
