/*============================================
DISCUSSION

MODULE: WD004-S8-DATA-STRUCTURE-3


SLICE 
extracts a section of a string and returns it as a new string, without modifying the original string.

SLICE METHOD HAS 2 PARAMETERS
1. beginIndex 
- where to begin extraction
- if negative, it is treated as str.length + beginIndex
- >= str.length, it returns an empty string
2. endIndex
- optional
- before which to end extraction
- if omitted, extracts to the end of the string
- if negative, it is treated as str.length + beginIndex

============================================*/

let str = 'Hellos';
let x = str.length;
console.log(`${str} has ${x} letters.`);
console.log(str.slice(0,1));	//H
console.log(str.slice(1,2));	//e
console.log(str.slice(2,3));	//1st length
console.log(str.slice(4,5));	//o  	or (4) only
console.log(str.slice(0,2));	//He
console.log(str.slice(1,4));	//ell


//REMOVE start and end letters
console.log("REMOVE start and end letters");
console.log(str.slice(1,-1));	//ello	 x + 0	//removed the 'H' and the 's' on 'Hellos'

console.log('***********************');
console.log(str.slice(0,1));		// show the first letter of string
console.log(str.slice(-1));			// show the last letter of string
console.log(str[0,1]);
console.log(str[-1]);







