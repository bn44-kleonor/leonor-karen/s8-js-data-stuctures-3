/*================================ 

ACTIVITY

Module: WD004-S7-DATA-STRUCTURES-2
GitLab: s7-data-structures-2

STACK 
A linear data structure that follows the mechanism: LAST IN, FIRST OUT.
===================================*/

//STEP 1: Define stack constructor function

// function Stack() {
								
// }

// let newStack = new Stack();			//instantiate



//STEP2: Create the following properties
//count with the initial value of 0
//storage - an empty object

// function Stack() {
// 	this.count = 0;							//this refer to 'this Stack'				
// 	this.storage = {};
// }

// let newStack = new Stack();			//instantiate 	//you created a Stack object called newStack
// console.log(newStack.count);		//namana from 'Stack' so nagkaroon din sya ng count and storage properties.
// console.log(newStack.storage);




//STEP3: Create the following methods. Instantiate the stack object to test it along the way.
//a) push - adds a value at the end of the stack (i.e.,storage property).
//CLUE: update value of count whenever you add a value at the end of the stack.

//*****start
// function Stack() {
// 	this.count = 0;							//this refer to 'this Stack'				
// 	this.storage = {};

// 	//adds a value at the end of the stack (i.e.,storage property).
// 	this.push = function (element)	{		// this is a method //function has no name because it is anonymous function or will not be called by its name (method)
// 		this.storage[this.count] = element;					
// 		this.count++;
// 	// element is the element that will be pushed inside the object (storage = {})									
// 	// storage  = { __:______ };
// 	// storage  = { _key/property_:__element__ };
// 	// storage  = { _0_:__1__ };			// key/property is the "0", element is the pushed "1"

// 	}
// }
//*****end


//b) pop - removes the value at the end of the stack and RETURNS the removed value
//CLUE 2: check value of count in relation to property. 


function Stack() {
	this.count = 0;							//this refer to 'this Stack' so it can be caled outside the function e.g. newStack.count			
	this.storage = {};

	//adds a value at the end of the stack (i.e.,storage property).
	this.push = function (element)	{
		this.storage[this.count] = element;					
		this.count++;
	}

	//emoves the value at the end of the stack and RETURNS the removed value
	this.pop = function() {
		let tempElem = this.storage[this.count-1];
		delete this.storage[this.count-1];
		this.count--;		// to update the value of count for use of 'length'
		return `The VALUE at the end of Stack that was deleted is "${tempElem}".`;
	}

	this.length = function (){
		return this.count;
	}

	this.peek = function (){
		return `The VALUE at the top of the stack is "${this.storage[0]}".`;		// "0" is name of property and not an index
	}
}




//c) length - REETURNS the number of values stored in the stack 
//d) peek - RETURNS the value at the top of the stack


//NOTE: storage is an object, not an array.


let newStack = new Stack();			//instantiate 	//you created a Stack object called newStack
console.log(newStack.count);		//namana from 'Stack' so nagkaroon din sya ng count and storage properties.
console.log(newStack.storage);
newStack.push("hello");				// push has "()" because it is a method
newStack.push("hi");
newStack.push("kumusta");
newStack.push("test");
console.log(newStack.storage);
console.log(newStack.count);
console.log(newStack.pop());
console.log(newStack.storage);
console.log("The Stack length is: " + newStack.length());
console.log(newStack.storage);
console.log(newStack.peek());
console.log(newStack.storage);
