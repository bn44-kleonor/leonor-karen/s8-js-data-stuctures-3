/*============================================
ACTIVITY

MODULE: WD004-S8-DATA-STRUCTURE-3
GitLab: s8-js-data-structure-3

Create a function that checks if a given work is a WORD palindrome or not.
A palindrome is a word, phrase, or sequence that reads the same backward as forward.
Examples are: madam, anna, hannah, racecar

Note: The function will be limited to checking WORD palindromes only.

1. Create a function called "checkPalindrome" that accepts a string as a parameter.
2. If the string has 1 or no character, display the console - It's a palindrome - and then return true. 
3. Otherwise, check if the first and last characters of the string are equal.
4. If they are, remove the first and last characters using slice method and return the same function (i.e., employ recursion). 
5. If they're not, display in the console - It's not a palindrome - and then return false.
============================================*/

//1
// let str = 'racecar';
// function checkPalindrome(str) {
// }

// abcdefgHgfedcba
//ABCDEFGhGFEDCBA

//2
//console.log("test");
//let str = "ABCDEFGhGfedcba";
let str = prompt("Enter a word.");
function checkPalindrome(str) {
	str = str.toLowerCase();
	if(str.length <= 1){
		console.log("it IS a palindrome");
		return true;
	} else {
		let tempStr = str;
		//console.log(`str is "${str}" ; str2 is "${str2}"`)
		//console.log
		if ( tempStr.slice(0,1) === tempStr.slice(-1)) {
			return checkPalindrome(str.slice(1,-1));
		} else {
			console.log("It's NOT a palindrome.");
			return false;
		}
	}
}

console.log(checkPalindrome(str));

